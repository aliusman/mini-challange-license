# Mini Challange License System

- Laravel 7
- JWT

## End point
1. Generate Token

```
Method : POST
url : {{ base_url }}/api/client
Param :
{
    app_type : WEB // harus WEB
    app_ip_server : 127.0.0.1 // IP client
    app_name : Lisence // harus sama dengan app name project
    app_domain : http://localhost:8000 // harus sama app url project
}

Response Code : 200
Response :
{
    "message": "Token generated successfully",
    "success": true,
    "token": "eyJhbGciOiJIU.zI1NiIsInR5cCI6Ik.pXVCJ9"
}
```
2. Validate Token

   Token di validasi via middleware dan harus memenuhi syarat seperti di atas.

```
Method : GET
url : {{ base_url }}/api/token?token=eyJhbGciOU.zI1NiIsInR5cC.pXVCJ
// token dapat di pass via query string seperti diatas, atau heade dengan key token
header :
{
    "token" : "eyJhbGciOU.zI1NiIsInR5cC.pXVCJ"
}

Response Code : 200
Response :
{
    "message": "Token retrieve successfully",
    "success": true,
    "token": "eyJhbGciOiJIU.zI1NiIsInR5cCI6Ik.pXVCJ9"
}
```