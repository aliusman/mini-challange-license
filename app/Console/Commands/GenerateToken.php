<?php

namespace App\Console\Commands;

use App\Traits\Token;
use Illuminate\Console\Command;

class GenerateToken extends Command
{
    use Token;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:token {ip} {type} {name} {domain}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $payload = [
            'app_name' => $this->argument('name'),
            'app_domain' => $this->argument('domain'),
            'app_type' => $this->argument('type'),
            'app_ip_server' => $this->argument('ip')
        ];

        $token = $this->generateNewToken($payload);

        $this->info($token);

        return 0;
    }
}
