<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\Token;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;

class LicenseController extends Controller
{
    use Token;
    /**
     * generate token
     * method POST
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function generateToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'app_name' => 'required|in:' . config('app.name'),
            'app_domain' => 'required|in:' . config('app.url'),
            'app_type' => 'required|in:WEB',
            'app_ip_server' => 'required|ip'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'success' => false,
                'errors' => $validator->errors()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // cek token is exists by ip
        if ($this->tokenExist($request->app_ip_server, $request->app_type)) {
            return response()->json([
                'message' => 'Token already exists',
                'success' => false
            ], Response::HTTP_CONFLICT);
        }

        Artisan::call('generate:token', [
            'ip' => $request->app_ip_server,
            'name' => $request->app_name,
            'domain' => $request->app_domain,
            'type' => $request->app_type
        ]);

        $token = $this->getToken($request->app_ip_server, $request->app_type);

        return response()->json([
            'message' => 'Token generated successfully',
            'success' => true,
            'token' => $token
        ], Response::HTTP_OK);
    }

    /**
     * Read token from file
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function showToken(Request $request)
    {
        return response()->json([
            'message' => 'Token retrieve successfully',
            'success' => true,
            'token' => $this->getToken($request->ip())
        ], Response::HTTP_OK);
    }
}
