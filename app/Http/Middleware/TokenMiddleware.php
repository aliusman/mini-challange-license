<?php

namespace App\Http\Middleware;

use App\Traits\Token;
use Closure;
use Illuminate\Http\Response;

class TokenMiddleware
{
    use Token;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // read token from header or query string
        $token = $request->header('token') ?? $request->query('token');

        if (!$this->validateToken($token, $request->ip())) {
            return response()->json([
                'message' => 'You are not authenticated',
                'error' => true
            ], Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }
}
