<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use ReallySimpleJWT\Token as JWT;

trait Token
{
    /**
     * generate new token
     *
     * @param array $payload
     *
     * @return string
     */
    protected function generateNewToken(array $payload)
    {
        $token = JWT::customPayload($payload, config('jwt.secret'));

        // save to file
        Storage::append('token/' . $this->argument('type') . '_' . $this->argument('ip') . '.txt', $token);

        return $token;
    }

    /**
     * Check token is exists (by ip and type)
     *
     * @param string $ip
     * @param string $type
     *
     * @return boolean
     */
    protected function tokenExist($ip, $type = 'WEB')
    {
        $path = 'token/' . $type . '_' . $ip . '.txt';

        if (Storage::exists($path)) {
            return true;
        }

        return false;
    }

    /**
     * Get token string from ip and type
     *
     * @param string $ip
     * @param string $type
     *
     * @return string
     */
    protected function getToken($ip, $type = 'WEB')
    {
        $path = 'app/token/' . $type . '_' . $ip . '.txt';
        $token = File::get(storage_path($path));

        return $token;
    }

    /**
     * Validate token
     *
     * @param string $token
     *
     * @return boolean
     */
    protected function validateToken($token, $ip = null)
    {
        $secret = config('jwt.secret');
        if (!JWT::validate($token, $secret)) {
            return false;
        }

        $payload = $this->decodeToken($token);

        // check key exist
        if (!isset($payload['app_name']) ||
            !isset($payload['app_type']) ||
            !isset($payload['app_domain']) ||
            !isset($payload['app_ip_server'])
        ) {
            return false;
        }

        // check app name is correct and domain and type
        if ($payload['app_type'] !== 'WEB' &&
            $payload['app_name'] !== config('app.name') &&
            $payload['app_domain'] !== config('app.url')
        ) {
            return false;
        }

        if ($ip && $payload['app_ip_server'] !== $ip) {
            return false;
        }

        // check token is exists
        if (!$this->tokenExist($payload['app_ip_server'], $payload['app_type'])) {
            return false;
        }

        return true;
    }

    /**
     * Decode token
     *
     * @param string $token
     *
     * @return array
     */
    protected function decodeToken($token)
    {
        $secret = config('jwt.secret');

        return JWT::getPayload($token, $secret);
    }
}
